<?php
function restCall($url, $port, $method, $data) {
  $ch = curl_init($url);
  
  $headers = array(
    'Content-Type: application/json',
    'Content-Length: '. strlen($data),
  );
  
  curl_setopt($ch, CURLOPT_HEADER, TRUE);
  curl_setopt($ch, CURLOPT_PORT, $port);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
  curl_setopt($ch, CURLINFO_HEADER_OUT, TRUE);
  
  $result = curl_exec($ch);
  curl_close($ch);
  
  return $result;
}

$req  = json_decode(file_get_contents('php://input'));
$data = (isset($req->data))? $req->data : '';
if (strpos($req->url, 'http://') === FALSE) {
  $req->url = 'http://'. $req->url;
}

//$req->port = 3000;
//$req->url  = "http://localhost/venues.json";
//$req->method = 'POST';
//$data = json_encode(array('name' => 'Nekkies', 'lat' => "34.1", 'long' => "18.4", 'blurb' => "vissmeer"));

if ($res = restCall($req->url, $req->port, $req->method, $data)) {
  list($headers, $content) = explode("\r\n\r\n", $res, 2);
  
  preg_match('/HTTP\/1\.1 (.+)/i', $headers, $code);
  
  $response = array(
    'code' => $code[1],
    'data' => $content,
  );
}
else {
  $response = array(
    'code' => 0,
    'data' => 'Request failed!',
  );
}

echo json_encode($response);