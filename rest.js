var app = angular.module('RESTDriver', []);

app.controller('MainCtrl', function($scope, $http) {
  $scope.options = ['GET', 'POST', 'PUT', 'DELETE'];
  $scope.operation = $scope.options[0];
  $scope.port = 80;
  $scope.working = false;
  
  $scope.doREST = function() {
    try {
      $scope.working = true;
      $http({
        method: 'POST',
        url: 'rest.php',
        data: {
          port: $scope.port,
          url: $scope.url,
          data: $scope.body,
          method: $scope.operation
        },
        headers: {'Content-Type': 'application/json'}
      }).success(function(response, code) {
        $scope.error = '';
        $scope.response = response.data;
        $scope.code = response.code;
        $scope.working = false;
      }).error(function(response, code) {
        $scope.error = "AJAX Error";
        $scope.response = response.data;
        $scope.code = response.code;
        $scope.working = false;
      });
    }
    catch (ex) {
      $scope.error = "Uncaught exception: ex";
    }
  }
});
